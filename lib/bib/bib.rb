module Bib
  	class Ref

		attr_accessor :autores, :titulo, :serie, :editorial, :numEdicion, :fecha, :numISBN
		
		def initialize(a,b,c,d,e,f,g)
			@autores = a
			@titulo = b
			@serie = c 
			@editorial = d
			@numEdicion = e
			@fecha = f
			@numISBN = g
		end

		def to_s()
			"#{autores.join(", ")}.\n#{titulo}\n(#{serie})\n#{editorial}; #{numEdicion} edicion #{fecha}\n#{numISBN.join("\n")}"

		end
	end
	
	class List
		
    	Node = Struct.new(:value, :next)
    	
		attr_accessor :head, :tail
		
		def initialize(ref)
			@head = Node.new(ref, nil)
			@tail = nil
		end
		
		def add (ref)
			if ref.instance_of? Ref
				if @head == nil
					@head = Node.new(ref, nil)
					@tail = nil
				else
					if @tail == nil
						@tail = Node.new(ref, nil)
						@head.next = @tail
					else
						aux = @tail
						@tail = Node.new(ref, nil)
						aux.next = @tail
					end
				end
			elsif ref.instance_of? Array
				ref.each do |i|
					self.add(i)
				end
			else 
				raise "Error en el parametro de add"
			end
		end
		
		def subtract
			x = self.head
			@head = x.next
			if @head.next == nil
				@tail = nil
			end
			return x.value
		end
	end
end


